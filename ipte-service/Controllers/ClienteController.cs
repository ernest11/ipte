﻿using ipte_service.Models;
using ipte_service.Utils;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Data;
using System.Data.Common;
using System.Text.Json.Serialization;

namespace ipte_service.Controllers
{
    [ApiController]
    [Route("cliente")]
    public class ClienteController : ControllerBase
    {
        [HttpGet]
        [Route("listar")]
        public dynamic listarClientes()
        {
            

            DataTable tCliente = DBConexion.Listar("Listar_Clientes");
            string jsonCliente = JsonConvert.SerializeObject(tCliente);
            return new
            {
                success = true,
                message = "Consulta exitosa",
                result = new
                {
                    cliente = JsonConvert.DeserializeObject<List<Cliente>>(jsonCliente)
                }
            };
        }

        [HttpGet]
        [Route("listarPorId")]
        public dynamic listarClientePorId(int id)
        {
            List<Parametro> parametros = new List<Parametro>
            {
                new Parametro("@idCliente", id.ToString())
            };

            DataTable tCliente = DBConexion.Listar("Listar_ClientePorId", parametros);
            string jsonCliente = JsonConvert.SerializeObject(tCliente);
            return new
            {
                success = true,
                message = "exito",
                result = new {
                    cliente = JsonConvert.DeserializeObject<List<Cliente>>(jsonCliente)
                }
            };
        }

        [HttpPost]
        [Route("guardar")]
        public dynamic guardarCliente(Cliente cliente)
        {
            List<Parametro> parametros = new List<Parametro>
            {
                new Parametro("@Nombre", cliente.nombre),
                new Parametro("@Correo", cliente.correo),
                new Parametro("@Telefono", cliente.telefono),
                new Parametro("@DescProblema", cliente.descProblema)
            };

            Boolean status = DBConexion.Ejecutar("Guardar_Cliente", parametros);
            string message = "";

            if (!status)
            {
                message = "Cliente registrado";
            }
            else
            {
                message = "Error al registrar Cliente";
            }
            
            return new
            {
                success = !status,
                message = message,
                result = ""
            };

        }

        [HttpPut]
        [Route("editar")]
        public dynamic editarCliente(Cliente cliente)
        {


             List<Parametro> parametros = new List<Parametro>
             { 
                new Parametro("@idCliente", cliente.idCliente.ToString()),
                new Parametro("@nombre", cliente.nombre),
                new Parametro("@correo", cliente.correo),
                new Parametro("@telefono", cliente.telefono),
                new Parametro("@descProblema", cliente.descProblema)
            };

            Boolean status = DBConexion.Ejecutar("Actualizar_Cliente", parametros);
            string message = "";

            if (status)
            {
                message = "Cliente actualizado";
            }
            else
            {
                message = "Error al actualizar cliente";
            }

            return new
            {
                success = status,
                message = message,
                result = ""
            };
        }
    }
}
