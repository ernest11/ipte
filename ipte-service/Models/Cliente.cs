﻿namespace ipte_service.Models
{
    public class Cliente
    {
        public long idCliente { get; set; }
        public string nombre { get; set; }
        public string correo { get; set; }
        public string telefono { get; set; }
        public string descProblema { get; set; }
    }
}
